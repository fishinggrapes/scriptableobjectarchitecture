﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "visualData", menuName = "Actor/Visual")]
public class ActorVisualData : ScriptableObject
{

    public Mesh mesh_ActorMesh;
    public Material[] mat_Materials;

    public RuntimeAnimatorController mPlayerAnimator;

    public AnimationClip mRunAnimation;

}
