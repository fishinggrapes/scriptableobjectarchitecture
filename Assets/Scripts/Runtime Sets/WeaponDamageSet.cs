﻿using UnityEngine;

[CreateAssetMenu(fileName = "newSet", menuName = "Sets/Damage Set")]
public class WeaponDamageSet : RunTimeSet<Damage>
{

}
