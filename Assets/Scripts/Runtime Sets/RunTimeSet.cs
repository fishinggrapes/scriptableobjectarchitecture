﻿using UnityEngine;
using System.Collections.Generic;

 public class RunTimeSet<T> : ScriptableObject
    {

        #region Private Fields and Properties

        public Dictionary<int, T> ObjectSet = new Dictionary<int, T>();

        #endregion

        #region Public Methods

        public void Add(int instanceID, T t)
        {
            if (!ObjectSet.ContainsKey(instanceID))
            {
                ObjectSet.Add(instanceID, t);
            }
        }

        public T Get(int instanceID)
        {
            if (ObjectSet.ContainsKey(instanceID))
            {
                return ObjectSet[instanceID];
            }

            return default(T);
        }

        public void Remove(int instanceID)
        {
            if (ObjectSet.ContainsKey(instanceID))
            {
                ObjectSet.Remove(instanceID);
            }
        }

        #endregion

}
