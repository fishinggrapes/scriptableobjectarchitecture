﻿using UnityEngine;
using QuickEvents;


    public class EventGroupListener : MonoBehaviour
    {

        #region Private Variables
        [SerializeField]
        [Tooltip("Event to register with.")]
        public EventGroup pEventGroup = null;

        [SerializeField]
        [Tooltip("Response to invoke when Event is raised.")]
        public AdvancedEvent Response = null;

        public int nSubGroup;
        public bool bUseInstanceID;
        public int nIndex;


        #endregion

        private void Awake()
        {
            if (bUseInstanceID)
            {
                nSubGroup = this.transform.root.gameObject.GetInstanceID();
            }
        }

        private void OnEnable()
        {
            pEventGroup.Register(nSubGroup, this);
        }

        private void OnDisable()
        {
            pEventGroup.Unregister(nSubGroup);
        }

        /// <summary>
        /// Calls the Response when Event is Fired
        /// </summary>
        public void OnEventRaised()
        {
            Response.Invoke();
        }
  
}