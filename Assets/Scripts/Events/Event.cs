﻿using UnityEngine;

namespace ScriptableEvent
{
    [CreateAssetMenu(fileName = "newEvent", menuName = "Event")]
    public class Event : ScriptableObject
    {

        #region private variables
        /// <summary>
        /// This Multicast Delegate is called when the associated event is fired in another MonoBehaviour 
        /// </summary>    
        private delegate void OnEventDelegate();
        private OnEventDelegate OnEventRaised;

        #endregion


        #region Event Functions

        /// <summary>
        /// Calls the Registered Callbacks
        /// </summary>
        public void Raise()
        {
            if (OnEventRaised != null)
                this.OnEventRaised();
        }

        /// <summary>
        /// Adds a Callback to the Multicast Delegate of Listeners
        /// </summary>
        /// <param name="listener"></param>
        public void Register(EventListener listener)
        {
            //Removes if Handler is already in the delegate, if so removes it
            OnEventRaised -= listener.OnEventRaised;

            //Add to the Delegate
            OnEventRaised += listener.OnEventRaised;

        }

        /// <summary>
        /// Removes the Callback from the Multicast Delegate
        /// </summary>
        /// <param name="listener"></param>
        public void Unregister(EventListener listener)
        {
            //Checked automatically for non Existing Handlers
            OnEventRaised -= listener.OnEventRaised;

        }

        #endregion

    }

}