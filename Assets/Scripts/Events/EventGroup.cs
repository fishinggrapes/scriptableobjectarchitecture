﻿using UnityEngine;
using System.Collections.Generic;


    [CreateAssetMenu(fileName = "newGroup", menuName = "Event Group")]
    public class EventGroup : ScriptableObject
    {

        #region private variables

        /// <summary>
        /// This Multicast Delegate is called when the associated event is fired in another MonoBehaviour 
        /// </summary>    
        private delegate void OnEventDelegate();
        private OnEventDelegate OnEventRaised;

        private Dictionary<int, OnEventDelegate> map_InstanceIDtoListeners = new Dictionary<int, OnEventDelegate>();

        #endregion


        #region Event Functions

        /// <summary>
        /// Calls the Appropriate Callbacks
        /// </summary>
        public void Raise(int SubGroupID)
        {
            map_InstanceIDtoListeners[SubGroupID]();
        }

        /// <summary>
        /// Adds a Callback to the Dictonary
        /// </summary>
        /// <param name="listener"></param>
        public void Register(int SubGroupID, EventGroupListener listener)
        {

            map_InstanceIDtoListeners.TryGetValue(SubGroupID, out OnEventRaised);

            OnEventRaised -= listener.OnEventRaised;
            OnEventRaised += listener.OnEventRaised;

            map_InstanceIDtoListeners[SubGroupID] = OnEventRaised;
            //map_InstanceIDtoListeners.Add(SubGroupID, OnEventRaised);
        }

        /// <summary>
        /// Removes the Callback from the Dictonary
        /// </summary>
        /// <param name="listener"></param>
        public void Unregister(int SubGroupID)
        {
            //Checked automatically for non Existing Handlers
            map_InstanceIDtoListeners.Remove(SubGroupID);

        }

#if UNITY_EDITOR

        public bool Contains(int SubGroupID)
        {
            //Checked automatically for non Existing Handlers
            return map_InstanceIDtoListeners.ContainsKey(SubGroupID);

        }
#endif

        #endregion

   

}