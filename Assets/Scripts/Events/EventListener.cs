﻿using UnityEngine;
using QuickEvents;
using Event = ScriptableEvent.Event;

public class EventListener : MonoBehaviour
{

    #region Private Variables
    [SerializeField]
    [Tooltip("Event to register with.")]
    public Event pEvent = null;

    [SerializeField]
    [Tooltip("Response to invoke when Event is raised.")]
    public AdvancedEvent Response = null;

    #endregion


    private void OnEnable()
    {
        pEvent.Register(this);
    }

    private void OnDisable()
    {
        pEvent.Unregister(this);
    }

    /// <summary>
    /// Calls the Response when Event is Fired
    /// </summary>
    public void OnEventRaised()
    {
        Response.Invoke();
    }

}