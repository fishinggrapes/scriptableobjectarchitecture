﻿using UnityEngine;

  [CreateAssetMenu(fileName = "Integer", menuName = "Primitives/Integer", order = 0)]
    public class Integer : Primitive<int>
    {
        
    }

