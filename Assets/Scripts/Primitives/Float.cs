﻿using UnityEngine;


    [CreateAssetMenu(fileName = "Float", menuName = "Primitives/Float", order = 1)]
    public class Float : Primitive<float>
    {
    }

