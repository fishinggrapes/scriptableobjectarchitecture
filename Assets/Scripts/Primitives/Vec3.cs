﻿using UnityEngine;

[CreateAssetMenu(fileName = "Vector3", menuName = "Primitives/Vector3", order = 5)]
public class Vec3 : ScriptableObject
{

    public float x;
    public float y;
    public float z;

    public Vector3 GetVector()
    {
        return new Vector3(x, y, z);
    }

    public Vector3 GetVector(Vector3 vector)
    {
        vector.x = x;
        vector.y = y;
        vector.z = z;

        return vector;
    }
}
