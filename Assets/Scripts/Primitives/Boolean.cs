﻿using UnityEngine;


[CreateAssetMenu(fileName = "Boolean", menuName = "Primitives/Boolean", order = 2)]
public class Boolean : Primitive<bool>
{

}

