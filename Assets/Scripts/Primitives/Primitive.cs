﻿using UnityEngine;
using MiscUtil;
using System.Collections.Generic;


    public abstract class Primitive<T> : ScriptableObject
    {

#if UNITY_EDITOR
        [Multiline]
        [SerializeField]
        private string Description;

#endif

        public T Value;

        public void Set(T value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            var primitive = obj as Primitive<T>;
            return
                   base.Equals(obj) &&
                   EqualityComparer<T>.Default.Equals(Value, primitive.Value);
        }

        public override int GetHashCode()
        {
            var hashCode = -159790080;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<T>.Default.GetHashCode(Value);
            return hashCode;
        }

        public static implicit operator T(Primitive<T> primitiveWrapper)
        {
            return primitiveWrapper.Value;
        }

        public static bool operator ==(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.Equals(primitive1.Value, primitive2.Value);
        }

        public static bool operator !=(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.NotEqual(primitive1.Value, primitive2.Value);
        }

        public static bool operator >=(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.GreaterThanOrEqual(primitive1.Value, primitive2.Value);
        }

        public static bool operator <=(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.LessThanOrEqual(primitive1.Value, primitive2.Value);
        }

        public static bool operator >(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.GreaterThan(primitive1.Value, primitive2.Value);
        }

        public static bool operator <(Primitive<T> primitive1, Primitive<T> primitive2)
        {
            return Operator<T>.LessThan(primitive1.Value, primitive2.Value);
        }


    }

