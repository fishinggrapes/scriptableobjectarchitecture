﻿using UnityEngine;

[CreateAssetMenu(fileName = "Vector2", menuName = "Primitives/Vector2", order = 4)]
public class Vec2 : ScriptableObject
{
    public float x;
    public float y;

    public Vector2 GetVector()
    {
        return new Vector2(x, y);
    }

    public Vector2 GetVector(Vector2 vector)
    {
        vector.x = x;
        vector.y = y;

        return vector;
    }
}
