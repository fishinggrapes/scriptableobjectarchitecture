﻿
public class Health
{
    public float Value;

    public Health(float MaxHealth)
    {
        Value = MaxHealth;
    }

    public void TakeDamage(float Damage)
    {
        Value -= Damage;
    }

    public override string ToString()
    {
        return Value.ToString();
    }
}