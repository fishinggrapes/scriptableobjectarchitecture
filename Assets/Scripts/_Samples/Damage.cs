﻿

public class Damage
{
    public float Value;

    public Damage()
    {
    }

    public Damage(float value)
    {
        Value = value;
    }

    public override string ToString()
    {
        return Value.ToString();
    }
}
