﻿using UnityEngine;
using TMPro;
using MEC;
using System.Collections.Generic;

[RequireComponent(typeof(TextMeshPro))]
public class UpdateText : MonoBehaviour
{
    [SerializeField]
    private PlayerHealthSet set_Health = null;

    private TextMeshPro txt_HealthBarks;
    private int nInstanceID;

    private void Awake()
    {
        nInstanceID = this.transform.root.gameObject.GetInstanceID();
    }
    // Use this for initialization
    void Start()
    {
        txt_HealthBarks = this.GetComponent<TextMeshPro>();
        txt_HealthBarks.SetText(set_Health.Get(nInstanceID).ToString());
    }

    public void Bark()
    {
        Debug.Log("Bark!");
        Timing.RunCoroutine(ExecuteAfterTime(0.01f));
    }

    private IEnumerator<float> ExecuteAfterTime(float delay)
    {
        yield return Timing.WaitForSeconds(delay);
        txt_HealthBarks.SetText(set_Health.Get(nInstanceID).ToString());
    }
}
