﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField]
    private WeaponDamageSet set_Damage = null;

    [SerializeField]
    private GameObject go_AttackedObject = null;

    [SerializeField]
    private int nSubGroupID = 0;

    [SerializeField]
    private EventGroup pActorShot = null;

    [SerializeField]
    private float fDamage = 0;

    [SerializeField]
    private bool bUseInstanceID = false;

    [SerializeField]
    private Float pDamage;


    private int nInstanceID;
    public void Attack()
    {

        nInstanceID = go_AttackedObject.GetInstanceID();
        set_Damage.Get(nInstanceID).Value = fDamage;
        pDamage.Value = fDamage;

        if (bUseInstanceID)
        {
            pActorShot.Raise(nInstanceID);
        }
        else
        {
            pActorShot.Raise(nSubGroupID);

        }
    }
}
