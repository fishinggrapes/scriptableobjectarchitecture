﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiredScript : EventfulMonoBehaviour, IHandle<Damage>, IHandle<Health>
{
    public void Handle(Damage message)
    {
        Debug.Log(message.Value);
    }

    public void Handle(Health message)
    {
        Debug.Log(message.Value);
    }
}
