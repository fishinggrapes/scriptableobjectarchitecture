﻿using QuickEvents;
using UnityEngine;

public class FiringScript : EventfulMonoBehaviour
{
    [SerializeField]
    private ActorVisualData pVisualData = null;

    [SerializeField]
    private GameObject mPlayerTemplate = null;


    //Important Link
    //https://docs.unity3d.com/ScriptReference/Animations.AnimatorController.html
    //https://www.pekalicious.com/blog/unity3d-reusing-animator-controllers-with-animatoroverridecontroller/
    void Start()
    {
        GameObject parentGO = new GameObject(pVisualData.name);
        parentGO.transform.position = new Vector3(0, 0, 25);

        GameObject go = GameObject.Instantiate<GameObject>(mPlayerTemplate, Vector3.zero, Quaternion.AngleAxis(270, Vector3.right)) as GameObject;
        go.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        go.GetComponent<MeshFilter>().mesh = pVisualData.mesh_ActorMesh;
        go.GetComponent<MeshRenderer>().materials = pVisualData.mat_Materials;

        go.transform.SetParent(parentGO.transform);
        parentGO.AddComponent<Animator>().runtimeAnimatorController = pVisualData.mPlayerAnimator;

    }
}
