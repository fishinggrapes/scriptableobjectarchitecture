﻿using UnityEngine;
using Event = ScriptableEvent.Event;

public class UiEVenst : MonoBehaviour
{

    [SerializeField]
    private EventGroup pButtonGroupEvent = null;

    [SerializeField]
    private Event pButtonEvent = null;

    private int nInstanceID;


    private void OnEnable()
    {
        nInstanceID = gameObject.GetInstanceID();
    }

    public void LogToConsole4()
    {
        Debug.Log("LogToConsole4!");
    }


    public void LogToConsole1()
    {
        Debug.Log("LogToConsole1!");
    }


    public void LogToConsole2()
    {
        Debug.Log("LogToConsole2!");
    }


    public void LogToConsole3()
    {
        Debug.Log("LogToConsole3!");
    }
    public void RaiseEvent()
    {
        pButtonGroupEvent.Raise(nInstanceID);
    }

    public void Raise()
    {
        pButtonEvent.Raise();
    }


}
