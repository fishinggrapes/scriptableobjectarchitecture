﻿using UnityEngine;

public class ActorOld : MonoBehaviour
{

    [SerializeField]
    private PlayerHealthSet set_Health = null;
    [SerializeField]
    private WeaponDamageSet set_Damage = null;

    private int nInstanceID;
    private Health mHealth;
    private Damage mDamage;

    [SerializeField]
    private Float pDamage;

    private void Awake()
    {
        nInstanceID = this.transform.root.gameObject.GetInstanceID();
        mHealth = new Health(100);
        mDamage = new Damage();
    }

    private void OnEnable()
    {
        set_Health.Add(nInstanceID, mHealth);
        set_Damage.Add(nInstanceID, mDamage);
    }

    public void Damage()
    {
        mHealth.Value -= pDamage.Value;
        Debug.Log(gameObject.name + " " + pDamage.Value + " " + mHealth.Value);
    }

    private void OnDestory()
    {
        set_Health.Remove(nInstanceID);
        set_Damage.Remove(nInstanceID);
    }
}
