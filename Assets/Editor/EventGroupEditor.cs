﻿using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(EventGroup))]
public class EventGroupEditor : Editor
{

    private EventGroup pEventGroup;
    private Texture2D tex2D_RaiseEvent;
    private GameObject go_CalledObject;
    private int nInstanceID;

    private void OnEnable()
    {
        pEventGroup = target as EventGroup;
        tex2D_RaiseEvent = Resources.Load<Texture2D>("Editor Gizmos/RaiseEvent");
    }

    public override void OnInspectorGUI()
    {

        GUILayout.BeginVertical();

        GUILayout.Space(15);
        go_CalledObject = EditorGUILayout.ObjectField("Raise Event on ", go_CalledObject, typeof(GameObject), true) as GameObject;
        GUILayout.Space(15);


        if (go_CalledObject != null)
        {
            nInstanceID = go_CalledObject.GetInstanceID();
            if (pEventGroup.Contains(nInstanceID))
            {
                GUILayout.BeginHorizontal();

                GUILayout.Label("Raise Event", GUILayout.MaxWidth(90));
                if (GUILayout.Button(tex2D_RaiseEvent))
                {
                    pEventGroup.Raise(nInstanceID);
                }

                GUILayout.EndHorizontal();

            }
            else
            {
                EditorGUILayout.HelpBox("The Selected GameObject is not in this EventGroup.", MessageType.Error, true);
            }

        }
        else
        {
            EditorGUILayout.HelpBox("Select a GameObject in this EventGroup.", MessageType.Warning, true);

        }

        GUILayout.EndVertical();
    }
}
