﻿using UnityEditor;
using Event = ScriptableEvent.Event;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(Event))]
public class EventEditor : Editor
{

    private Event pEvent;
    private Texture2D tex2D_RaiseEvent;

    private void OnEnable()
    {
        pEvent = target as Event;
        tex2D_RaiseEvent = Resources.Load<Texture2D>("Editor Gizmos/RaiseEvent");
    }

    public override void OnInspectorGUI()
    {
        GUILayout.Space(15);
        GUILayout.BeginHorizontal();

        GUILayout.Label("Raise Event", GUILayout.MaxWidth(90));
        if (GUILayout.Button(tex2D_RaiseEvent))
        {
            pEvent.Raise();
        }

        GUILayout.EndHorizontal();
    }
}
