﻿using UnityEditor;
using UnityEngine;

    [CanEditMultipleObjects]
    [CustomEditor(typeof(EventGroupListener))]
    public class EventGroupListenerEditor : Editor
    {
        private SerializedProperty pEventGroup;
        private SerializedProperty nSubGroupID;
        private SerializedProperty mResponse;
        private SerializedProperty bUseInstanceID;
        private SerializedProperty nIndex;

        private GUIContent mIDLabel;
        private string[] str_Options = new string[] { "Root InstanceID", "Group ID" };

        private void OnEnable()
        {
            pEventGroup = serializedObject.FindProperty("pEventGroup");
            mResponse = serializedObject.FindProperty("Response");
            nSubGroupID = serializedObject.FindProperty("nSubGroup");
            bUseInstanceID = serializedObject.FindProperty("bUseInstanceID");
            nIndex = serializedObject.FindProperty("nIndex");

            mIDLabel = new GUIContent("Group ID");
        }

        public override void OnInspectorGUI()
        {
            //DrawDefaultInspector();
            serializedObject.Update();

            EditorGUILayout.BeginVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            pEventGroup.objectReferenceValue = EditorGUILayout.ObjectField("Event", pEventGroup.objectReferenceValue, typeof(EventGroup), true) as ScriptableObject;

            EditorGUILayout.Space();
            nIndex.intValue = EditorGUILayout.Popup("Group by ", nIndex.intValue, str_Options);

            switch (nIndex.intValue)
            {
                case 0:
                    bUseInstanceID.boolValue = true;
                    EditorGUILayout.HelpBox("Gameobjects would use their Root GameObject's InstanceID as their Group ID.", MessageType.Info, true);
                    break;

                case 1:
                    bUseInstanceID.boolValue = false;
                    EditorGUILayout.HelpBox("Gameobjects would use an user-defined Integer as their Group ID.", MessageType.Info, true);
                    EditorGUILayout.PropertyField(nSubGroupID, mIDLabel);
                    break;
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(mResponse);

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }
    }
