﻿using UnityEditor;
using UnityEngine;

    [CanEditMultipleObjects]
    [CustomEditor(typeof(EventListener))]
    public class EventListenerEditor : Editor
    {
        private SerializedProperty pEvent;
        private SerializedProperty mResponse;

        private void OnEnable()
        {
            pEvent = serializedObject.FindProperty("pEvent");
            mResponse = serializedObject.FindProperty("Response");

        }

        public override void OnInspectorGUI()
        {
            //DrawDefaultInspector();
            serializedObject.Update();

            EditorGUILayout.BeginVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            pEvent.objectReferenceValue = EditorGUILayout.ObjectField("Event", pEvent.objectReferenceValue, typeof(Event), true) as ScriptableObject ;

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(mResponse);

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }
    
}
